extends Node2D

export var speed = 300 #exportilla nämä variablet näkee editorissa ja niitä voi myös muokata muualta
export var korkeus = 0
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


func _process(delta):
	for i in range(0, self.get_child_count()): #loop ajetaan niin monta kertaa kuin palkit noden alla on child nodeja
		var velocity = Vector2(-1*speed,0) # vaakasuora nopeus on negatiivinen ja se kerrotaan speedillä, pystysuoranopeus on 0
		get_child(i).position += velocity * delta #palkin position muutetaan nopeus kerrottuna deltalla mikä tekee liikkeestä tasaista frameratesta riippumatta
	
		if(get_child(i).position.x<-960): # tässä katsotaan onko palkit peliruudun ulkopuolella ja jos on ne siirretään takaisin oikealle
			get_child(i).position = Vector2(960,get_child(i).position.y)
	
