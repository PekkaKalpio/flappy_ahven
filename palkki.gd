extends Area2D

export var speed = 300 #exportilla nämä variablet näkee editorissa ja niitä voi myös muokata muualta
export var korkeus = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _process(delta):
	var velocity = Vector2(-1*speed,0) # vaakasuora nopeus on negatiivinen ja se kerrotaan speedillä, pystysuoranopeus on 0
	position += velocity * delta #palkin position muutetaan nopeus kerrottuna deltalla mikä tekee liikkeestä tasaista frameratesta riippumatta
	
	if(position.x<-20): # tässä katsotaan onko palkit peliruudun ulkopuolella ja jos on ne siirretään takaisin oikealle
		korkeus = rand_range(-100, 50) # korkeus randomoidaan
		position = Vector2(760,korkeus)



func _on_palkki_body_entered(body): #signaalin funktio 
	if(body.get_name() == "kala"): #katsotaan oliko osuja pelaaja
		print(body.get_name() +" hit " + self.get_name() + "!")
		get_parent().get_parent().end_game() #kutsutaan pelaajan pelin lopetus funktiota
	return
