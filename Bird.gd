extends RigidBody2D


var velocity = Vector2.ZERO

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.	


func _process(_delta):
	if rad2deg(get_rotation()) < -30: #laitetaan pelaajalle maksimi ylöspääinrotaatio angle 30 astetta
		set_rotation(deg2rad(-30))
		set_angular_velocity(0)

	if get_linear_velocity().y > 0: # jos pelaaja tippuu alaspäin sitä aletaan kääntämään myös alaspäin
		set_angular_velocity(1.5)


func _input(_event): #kutsutaan kun pelaaja painaa näppäintä
	if Input.is_action_pressed("flap") and self.sleeping == false: #action flap on merkttu project > settings > input, katsotaan myös onko pelaaja aktiivinen
		flap()
	return
	

func flap():
	set_linear_velocity(Vector2(0,-250)) # pelaajan rigidbodyyn laitetaan -250 pystysuoraa nopeutta
	set_angular_velocity(-3) # ja -3 angulaarista velocityä mikä kierrättää hahmoa
	return

