extends Node2D

var points = 0  #pisteiden numeerinen arvo
var score = Label  #tekstikenttä mihin pisteet laitetaan
var palkit #node minkä alla palkit ovat
var active = false # pelin aktiivisuus boolean
var valikko #interface node
var kala # pelaaja
var takatausta
var etutausta


# Called when the node enters the scene tree for the first time.
func _ready():
	score = get_node("pisteet") #määritellään oikeat nodet variableihin, get nodella voi etsiä nimellä ja get_child numerolla kala on 1 koska arrayt alkaa 0
	valikko = get_node("valikko")
	kala = get_child(1)
	palkit = get_node("palkit")
	takatausta = get_node("takatausta")
	etutausta = get_node("etutausta")
	set_palkki_nopeus(0) # kutsutaan funktiota set_palkki_nopeus joshon laitetaan muuttujaksi 0 mikä pysäyttää palkkien liikkeen
	

func _process(delta):
	if(active): # kun peli on aktiivinen pisteihin lisätään delta mikä on framejen välissä oleva välin pituus
		points += delta
		score.text = "points: " + str(int(points)) # tekstikenttää päivitetään str muuttaa ensin floatin täysiksi numeroiksi ja sitten se muutetaan string muotoon labelia varten


func start_game(): #pelin aloitusfunktio
	valikko.hide()# aloitus nappi piilotetaan ja peli laitetaan aktiiviseksi
	active = true
	set_palkki_nopeus(300) # palkit laitetaan liikkumaan
	kala.sleeping = false # rigidbody nukkumistila pois päältä joten kala alkaa tippumaan

func new_game(): # kutsutaan kun peli loppuu ja pelaaja haluaa pelata peliä uudelleen
	print("ladataan skene uudelleen")
	get_tree().reload_current_scene() # skene ladataan uusiksi mikä resetöi kaikkien osien tilan


func end_game(): # kutsutaan kun pelaaja osuu palkkiin
	active = false; 
	valikko.get_child(1).text = "SCORE = " + str(int(points)) + " \n \n \n"
	valikko.show()
	kala.sleeping = true
	set_palkki_nopeus(0)
	set_tausta_nopeus(0,takatausta)
	set_tausta_nopeus(0,etutausta)


func set_palkki_nopeus(speed): 
	for i in range(0, palkit.get_child_count()): #loop ajetaan niin monta kertaa kuin palkit noden alla on child nodeja
		print("laitetaan palkki " + str(i) + " nopeus " + str(speed))
		palkit.get_child(i).speed = speed

func set_tausta_nopeus(speed, objeketi): 
	objeketi.speed = speed

func _on_Button_pressed(): #tässä tarkastetaan mitä start nappi tekee, jos pisteitä on enemmän kuin 0 eli jos peli ei ole resetöity kutsutaan skenen reloadaavaa funktiota
	if(points > 0): 
		new_game()
	else:
		start_game()
	return 

